{-# LANGUAGE EmptyDataDecls #-}
{-# LANGUAGE OverloadedStrings #-}

module HTICjs where

import FFI
import DOM
import Data.Maybe 
import Data.List
import Data.Either 

import Data.Text hiding (head,drop,init,map,unlines,isPrefixOf,concat,any,putStrLn,concatMap,length
                        ,last,reverse,take)
import qualified Data.Text as T



data Branch = B {
    bname   :: String
   ,boption :: String
   ,boptionChosenState :: Maybe String
   ,bcontent :: String
   ,bnext    :: [String]
   ,bdisplayOnNewPage :: Bool
   ,bautoContinue :: Maybe Int
   ,baltContent :: Maybe String
   ,baltContentTimeout :: Maybe Int
   ,brandomNext :: Bool
   ,bbuildFromTemplate :: Maybe [(String, String)]
   ,boverrideNext :: Maybe String
   ,bskipPastNext :: [[Int]]
   ,brunCode :: Maybe String
   }


emptyBranch = B {
    bname   = "branchEmpty"
   ,boption = ""
   ,boptionChosenState = Nothing
   ,bcontent = ""
   ,bnext    = []
   ,bdisplayOnNewPage = False
   ,bautoContinue = Nothing
   ,baltContent = Nothing
   ,brandomNext = False
   ,bbuildFromTemplate = Nothing
   ,boverrideNext = Nothing
   ,bskipPastNext = []
   ,brunCode = Nothing
   }


data Context = C {
    cbranches :: [Branch]
   ,cAutoContinueTimers :: [Timer]
   ,cEdit :: Branch
   ,cDontUpdateEditListener :: Bool
   ,cDontRemoveOptions :: Bool
   }

emptyContext = C {
    cbranches = []
   ,cAutoContinueTimers = []
   ,cEdit = emptyBranch {bname = "edit", bdisplayOnNewPage = True}
   ,cDontUpdateEditListener = False
   ,cDontRemoveOptions = False
   }

(^.) d a = a d
setCbranches d c = c {cbranches = d}
setCautoContinueTimers d c = c {cAutoContinueTimers = d}


addWindowEvent :: String -> (Event -> Fay a) -> Fay a
addWindowEvent = ffi "window.addEventListener(%1, %2)"

-- | Add an event listener to an element.
addEventListener :: a -> String -> Fay () -> Bool -> Fay ()
addEventListener = ffi "%1['addEventListener'](%2,%3,%4)"

appendChild' :: Element -> Element -> Fay ()
appendChild' = ffi "%1.appendChild(%2)"

createElement' :: String -> Fay Element
createElement' = ffi "document.createElement(%1)"


getInnerHtml :: Element -> Fay String
getInnerHtml = ffi "%1['innerHTML']"

getOuterHtml :: Element -> Fay String
getOuterHtml = ffi "%1['outerHTML']"

getInnerHtml' :: Element -> Fay [Element]
getInnerHtml' e = return . nodeListToArray =<< children e

setInnerHtml :: Element -> String -> Fay Element
setInnerHtml = ffi "%1['innerHTML'] = %2"

setOuterHTML :: Element -> String -> Fay Element
setOuterHTML = ffi "%1['outerHTML'] = %2"

setAttribute :: Element -> String -> String -> Fay Element
setAttribute = ffi "%1.setAttribute(%2, %3)"

--setStyle :: Element -> String -> String -> Fay ()
--setStyle = ffi ("%1.style.%2 = %3")


getAttribute :: Element -> String -> Fay String
getAttribute = ffi "%1[%2]"

hasAttribute :: Element -> String -> Fay Bool
hasAttribute = ffi "%1[%2]"

removeAttribute :: Element -> String -> Fay Element
removeAttribute = ffi "%1.removeAttribute(%2)"

removeElement :: Element -> Fay ()
removeElement = ffi "%1.remove()"

getLastChild :: Element -> Fay Element
getLastChild = ffi "%1['lastChild']"

getFirstChild :: Element -> Fay Element
getFirstChild = ffi "%1['firstChild']"

replaceChild :: Element -> Element -> Element -> Fay Element
replaceChild = ffi "%1.replaceChild(%2,%3)"

childElementCount :: Element -> Fay Int
childElementCount = ffi "%1['childElementCount']"

children_l e = return . nodeListToArray =<< children e


hasChildNodes :: Element -> Fay Bool
hasChildNodes = ffi "%1['hasChildNodes']"

cloneNode :: Element -> Bool -> Fay Element
cloneNode = ffi "%1.cloneNode(%2)"


getElementById' :: String -> Fay Element
getElementById' = ffi "document.getElementById(%1)"       

getAllWithGivenValue :: String -> Fay [(String,Element)]
getAllWithGivenValue v = do 
   flip getAllWithGivenValueFrom v =<< getBody
   
getAllWithGivenValueFrom :: Element -> String -> Fay [(String,Element)]
getAllWithGivenValueFrom e v = do 
   getAllWithValueMatchingPredicateFrom e (v ==)

getAllWithGivenValueFrom_l :: [Element] -> String -> Fay [(String,Element)]
getAllWithGivenValueFrom_l e v = do 
   getAllWithValueMatchingPredicateFrom_l e (v ==)  

getAllWithValueMatchingPredicateFrom :: Element -> (String->Bool) -> Fay [(String,Element)]
getAllWithValueMatchingPredicateFrom e v = do 
   flip getAllWithValueMatchingPredicateFrom_l v =<< children_l e


getAllWithValueMatchingPredicateFrom_l :: [Element] -> (String->Bool) -> Fay [(String,Element)]
getAllWithValueMatchingPredicateFrom_l e v = do 
   d<- getAllWithValue_from_l e
   dv<- mapM (flip getAttribute "value") d
   return $ filter (v . fst) $ zip dv d

{-getAllWithValueMatchingPredicateFrom_l_ve :: [Element] -> (String->Bool) -> Fay [Element]
getAllWithValueMatchingPredicateFrom_l_ve e v = do 
   d<- getAllWithValue_from_l e
   dv<- mapM (flip getAttribute "value") d
   return $ filter (v . fst) $ zip dv d
-}
offsetHeight :: Element -> Fay Int
offsetHeight = ffi "%1['offsetHeight']"


window_innerHeight :: Fay Int
window_innerHeight = ffi "window.innerHeight"

setTimeout' :: (Fay ()) -> Int -> Fay Timer
setTimeout' = ffi "window.setTimeout(%1, %2)"

clearTimeout' :: Timer -> Fay ()
clearTimeout' = ffi "window.clearTimeout(%1)"

alert :: String -> Fay ()
alert = ffi "window.alert(%1)" 

getRandom :: [a] -> Fay a
getRandom = ffi "randomItem(%1)"

shuffle :: [a] -> Fay [a]
shuffle = ffi "shuffle(%1)"

scrollIntoView' :: Element -> Bool -> Fay ()
scrollIntoView' = ffi "%1.scrollIntoView(%2)"

scrollIntoView'' :: Element -> Bool -> Fay ()
scrollIntoView'' e b = do 
  doRun <- (return.fromMaybe 0) 
         =<< getIntSetting "scrollIntoView" 
         =<< children_l 
         =<< getElementById' "settings"

  when (doRun == 1) $ scrollIntoView' e b

runCode :: String -> Fay ()
runCode code = do 
  putStrLn ("runCode ffi: " ++ (take 100 code))
  runCode' code

runCode' :: String -> Fay ()
runCode' = ffi "runCode(%1)" 

sanityCheck allBranches = do
   putStrLn "sanityCheck"
   mapM_ (\(from,to)-> putStrLn $ "unreachable " ++ from ++ "->" ++ to) 
         $ concatMap checkBranch allBranches
   
   let allLinks = concatMap (\bb-> bb^.bnext) allBranches
   
   allOptionsOnDisplay<- return . map (fromJust . stripPrefix "option_" . fst) 
                           =<< flip getAllWithValueMatchingPredicateFrom ("option" `isPrefixOf`) 
                           =<< getElementById' "display"
                                                            
   mapM_ (\bb-> putStrLn $ "nothing points to " ++ bb) 
      $ checkBranchReachebility allOptionsOnDisplay allBranches

   mapM_ printNameList $ checkIfBrancheNamesAreUnique allBranches
   
   putStrLn "sanityCheck done"
   where
   checkBranch b = 
      zip (repeat $ b^.bname)
         $ map fst
         $ filter (isNothing . snd)
         $ zip (b^.bnext)
               $ map (getBranchByName allBranches) $ b^.bnext
   
   checkBranchReachebility allOptionsOnDisplay allBranches = 
      map fst
         $ filter (isNothing . snd)
         $ zip (map (\bb-> bb^.bname) allBranches)
               $ map   (\bb-> find (bb^.bname ==) 
                                    $ conc allOptionsOnDisplay 
                                          $ concatMap (\bb-> bb^.bnext) allBranches
                        ) allBranches 
      
   checkIfBrancheNamesAreUnique allBranches = 
      nub $ filter (\l-> length l >1) 
         $ map (\n-> filter (n==) (allNames allBranches) ) 
               $ allNames allBranches
     
   allNames allBranches = map (^.bname) allBranches
   
   printNameList n 
      |l>1 = putStrLn $ "not unique " ++ (head n) ++ " (" ++ (show l) ++ ")"
      |otherwise = return ()
      where
      l = length n

initContext allBranches = do 
   let c = setCbranches allBranches emptyContext
   return c


setDisplay :: Fay ()
setDisplay = do 
   allBranches<- parseAllBranches
   sanityCheck allBranches
   c<- initContext allBranches
   allOptions<- flip getAllWithValueMatchingPredicateFrom ("option" `isPrefixOf`) 
                                                            =<< getElementById' "display"

   setAllowedListeners allOptions c allBranches
              
   addEventListenerToPagesTop    c =<< children_l =<< getElementById' "pagesTop"
   addEventListenerToPagesBottom c =<< children_l =<< getElementById' "pagesBottom"
                  
   
   (\e-> addEventListener e 
                          "click" 
                          removeHistory
                          True
      ) =<< getElementById' "clearHistory"
   
   
   where 
   
   setAllowedListeners allOptions c allBranches = do 
      forbidden <- flip hasValueSet "forbidListenersOnDisplay" =<< getElementById' "cache"
      
      case forbidden of
         True -> return ()
         _    -> mapM_ (\(id,o)-> do 
                        let bb = fromJust $ getBranchByName id allBranches
                        o<- getFirstChild o
                        addEventListener o 
                                       "click" 
                                       (displayBranch c bb) 
                                       True
                       ) 
                   
                   $ filter (idMatchesOneOfBranches allBranches) 
                   $ map (\(id,o)-> ((fromJust $ stripPrefix "option_" id), o))
                   $ allOptions
                         
             
   addEventListenerToPagesTop    c l = addEventListenerToPages c True  l
   addEventListenerToPagesBottom c l = addEventListenerToPages c False l
   addEventListenerToPages c tb l = 
      mapM_ (\p-> do n<- parseInt . pack =<< getInnerHtml p
                     addEventListener p 
                                    "click" 
                                    (displayPage c (scrollIntoView'' p tb) (n-1)) 
                                    True
            ) l
   
   idMatchesOneOfBranches allBranches = (flip elem (map (^.bname) allBranches) . fst) 
   
   getBranchByName id b = find (\b-> id==b^.bname) b
   
   giveAllOptions_inDisplay = getAllWithValue_from =<< getElementById' "display"
   
   head'  = fromMaybe emptyBranch . listToMaybe 
   
head' default_ = fromMaybe default_ . listToMaybe 


getAllWithValue_fromDisplay = 
   getAllWithValue_from =<< getElementById' "display"
              
              
getAllWithValue_from element = getAllWithValue_from_l =<< children_l element

getAllWithValue_from_l list = 
   return 
   . map snd
   . filter fst 
   =<< mapM (\e-> do hvalue<-hasAttribute e "value"
                     return (hvalue, e)
            ) list


displayBranch :: Context -> Branch -> Fay ()
displayBranch c b = do 

   c'<-clearAllAutoContinueTimers c
   let c = c'

   b' <- overrideNext c b
   let b = b'

   c' <- case (b^.bbuildFromTemplate) of
           Nothing -> return c
           Just _  -> return $ c {cDontRemoveOptions = True}
   let c = c'

   whenJust (b^.bbuildFromTemplate) $ buildFromTemplate c

   sp (c {cDontUpdateEditListener = False}) =<< getElementById' "display"
   
   sd (c {cDontUpdateEditListener = False}) $ b^.bcontent
   
   let c' = c {cDontRemoveOptions = False}
   let c = c'  
   
   lastContent <- getLastChild =<< getElementById' "display"
               
   act<- return . maybeToList 
            =<< whenJust' 
                  (b^.bautoContinue) 
                  (setTimeout' (displayNextBranch b $ c {cDontUpdateEditListener = False}) )
   let c' = setCautoContinueTimers (conc act $ c^.cAutoContinueTimers) c
   let c = c'
   let c' = c {cDontUpdateEditListener = False}
   let c = c'   
   
   
   
   sdo c b =<< getElementById' "display"
   
   updateLastPage
   
   _<-whenJust' (b^.baltContent) 
            $ (\_-> setTimeout' (displayAlternativeContent b lastContent) 
                           $ fromMaybe 5000 
                                       $ b^.baltContentTimeout
               )

   whenJust (b^.brunCode) runCode
   
   return ()
   
   where       
   
   updateLastPage :: Fay ()
   updateLastPage = do 
      last <- getLastChild =<< getElementById' "pages"
      setInnerHtml last =<< getInnerHtml =<< getElementById' "display"
      return ()
   
   --savePage :: Fay ()
   
   savePage c = do 
         p<- getElementById' "pages"
         appendChild p =<< newPage =<< getInnerHtml =<< getElementById' "display"
         
         n<- childElementCount p
         npl <- newPageLink n
         npl'<- cloneNode npl True
         
         flip appendChild npl =<< getElementById' "pagesTop"
         flip appendChild npl' =<< getElementById' "pagesBottom"
         
         addEventListener npl 
                        "click" 
                        (displayPage c (scrollIntoView'' npl True) (n-1)) 
                        True
                        
         addEventListener npl' 
                        "click" 
                        (displayPage c (scrollIntoView'' npl' False) (n-1)) 
                        True
      where
      newPage content = do 
         e<-createElement' "div"
         setInnerHtml e content
         return e
      
      newPageLink number = do 
         e<-createElement' "button"
         --setAttribute e "style" $ "margin:1em;"
         setAttribute e "class" $ "otherPage"
         setInnerHtml e $ show number
         return e
      
         
   sp c display = do 
      wh<- window_innerHeight
      dh<- offsetHeight display
      
      savePageAndClearIfTooBig ((fromIntegral dh)/(fromIntegral wh))
         . fromMaybe 10 
         =<< getIntSetting "newPageThreshold" 
         =<< children_l 
         =<< getElementById' "settings"
      
      where
      savePageAndClearIfTooBig :: Double -> Int -> Fay ()
      savePageAndClearIfTooBig h threshold
         |h> fromIntegral threshold = saveContentAndStartNewPage
         |b^.bdisplayOnNewPage      = saveContentAndStartNewPage
         |otherwise                 = return ()
         
         where
         saveContentAndStartNewPage = do 
            removeUnusedOptions
            updateLeftoverAltContents c
            flip whenJust removeElement . listToMaybe . reverse
               =<< children_l =<< getElementById' "pages"
            flip whenJust removeElement . listToMaybe . reverse
               =<< children_l =<< getElementById' "pagesTop"
            flip whenJust removeElement . listToMaybe . reverse
               =<< children_l =<< getElementById' "pagesBottom"
            savePage c
            clearInnerHtml display
            savePage c
      
   
   parseUsedUnusedOptions = return . partition (("option_" ++ (b^.bname) ==) . fst) 
                              =<< flip getAllWithValueMatchingPredicateFrom ("option" `isPrefixOf`) 
                              =<< getElementById' "display"
   
   removeUnusedOptions = do 
      mapM_ (removeElement . snd) . snd =<<  parseUsedUnusedOptions
   
   removeUnusedOptions_un  = mapM_ (removeElement . snd) 
      
   
   
         
   sd :: Context -> String -> Fay ()
   --sd = ffi "document.getElementById(\"display\").innerHTML = %1"    
   sd c newContent = do   
      
      
      e<-wrapContent (b^.bname) newContent
      
      (usedOptions,unusedOptions)<- parseUsedUnusedOptions
      
      when (not $ c^.cDontRemoveOptions) $ do
         removeUnusedOptions_un unusedOptions
        
         mapM_ (\(n,o)->do  setAttribute o 
                                    "value" 
                                    $ "content_chosen_" ++ (fromJust $ stripPrefix "option_" n)
                            useChosenStateIfAvailble o
               ) usedOptions
               
         mapM_ (flip removeAttribute "id") . concat =<< mapM (children_l . snd) usedOptions
      
      
      flip appendChild e =<< getElementById' "display"
      
      scrollIntoView'' e True
      
      where
      wrapContent branchName content = do 
         e<-createElement' "data"
         setAttribute e "value" $ "content_" ++ branchName
         setInnerHtml e content
         return e
      
      
      useChosenStateIfAvailble o = step1 $ b^.boptionChosenState
        where
        step1 Nothing = do
          oo<-getInnerHtml o
          clearInnerHtml o
          setInnerHtml o oo
          
        step1 (Just oc) = setInnerHtml o oc
      
     
   
   sdo c b display 
      |b^.brandomNext = common =<< (shuffle $ b^.bnext)
      |otherwise      = common $ b^.bnext
      
      where
      
      wrapOption branchName content = do 
         e<-createElement' "data"
         setAttribute e "value" $ "option_" ++ branchName
         setInnerHtml e content
         f<-getFirstChild e
         --setAttribute f "id" branchName
         return e
         
      common next = mapM_ (\bb->do 
                           e<-wrapOption (bb^.bname) $ bb^.boption
                           appendChild display e
                           
                           --o<-getElementById' $ bb^.bname 
                           o<- getFirstChild =<< getLastChild display
                           addEventListener o 
                                            "click" 
                                            (displayBranch c bb) 
                                            True
                           
                       ) 
                        $ catMaybes 
                        $ map (getBranchByName (c^.cbranches)) next
                              
   
   displayAlternativeContent b content = do 
      setAttribute content "value" $ "content_alt_" ++ (b^.bname)
      setInnerHtml content $ fromMaybe "" $ b^.baltContent
      updateLastPage
   
   
   displayNextBranch b c = do
      
      case (b^.brandomNext) of 
         False -> safeHeadGo (b^.bnext) common
         True  -> flip safeHeadGo common =<< (shuffle $ b^.bnext)
         
      where
      common = (flip whenJust $ displayBranch c) . getBranchByName (c^.cbranches)                                   
      safeHeadGo l go = whenJust (listToMaybe l) go
         
          

removeHistory :: Fay ()
removeHistory = do
   clearInnerHtml =<< getElementById' "pages"
   clearInnerHtml =<< getElementById' "pagesTop"
   clearInnerHtml =<< getElementById' "pagesBottom"
   --setDisplay

    
removeAllEventListenersOnNode e = do
   t<-cloneNode e True
   setOuterHTML e =<< getOuterHtml t
   removeElement t
      
      

updateElementByIdAndOverwriteListeners name content event doOnEvent = do
   e<- getElementById' name
   flip setInnerHtml content e
   removeAllEventListenersOnNode e
   e<- getElementById' name
   addEventListener e 
                    event
                    doOnEvent
                    True

   
   

   
   
   
updateLeftoverAltContents c = do
   let allWithAltContent = filter (\B{baltContent=a}-> isJust a) $ c^.cbranches
      
   mapM_ (\(_,e,b)-> dac b e)
      . map (\(_1,_2,_3)-> (_1,_2,fromJust _3)) 
      . filter (\(_,_,_3)-> isJust _3)
      =<< mapM (matchElementWithBranch allWithAltContent)
      =<< mapM (\e-> return . (\v->(v,e)) =<< (getAttribute e "value"))
      . concat
      =<< mapM children_l
      . take 4
      . reverse
      =<< children_l 
      =<< getElementById' "pages"
      

   where
   matchElementWithBranch allWithAltContent (v,e)  = do
      let branch = find (\b-> (stripPrefix "content_" v) == (Just $ b^.bname)) allWithAltContent
      --putStrLn $ unwords ["matchElementWithBranch", v]
      return (v,e,branch)
      
   
   dac b e = do
      void $ setAttribute e "value" $ "content_alt_" ++ (b^.bname)
      void $ setInnerHtml e $ fromMaybe "" $ b^.baltContent
      --print =<< getValue e
      
clearAllAutoContinueTimers c = do
   mapM_ clearTimeout' $ c^.cAutoContinueTimers
   let c' = setCautoContinueTimers [] c
   return c'
   
   
displayPage :: Context -> Fay () -> Int -> Fay ()
displayPage c alignScroll n = do 
   
   c'<-clearAllAutoContinueTimers c
   let c = c'
   
   updateLeftoverAltContents c   
   
   p<- getInnerHtml . flip  (!!) n =<< children_l =<< getElementById' "pages"
   mapM_ (stylePages n) . zip [0..] =<< children_l =<< getElementById' "pagesTop"
   mapM_ (stylePages n) . zip [0..] =<< children_l =<< getElementById' "pagesBottom"
   
   
   
   flip setInnerHtml p =<< getElementById' "display"
   
   totalPages<- childElementCount =<< getElementById' "pages"
   
   case n+1 == totalPages of
      False -> setForbidListenersOnDisplay
      True  -> do 
         unsetForbidListenersOnDisplay
         setDisplay
   
   alignScroll
   
   
   where
   stylePages n (m,p)   
      |n==m      = setAttribute p "class" "currentPage"
      |otherwise = setAttribute p "class" "otherPage"
         

setForbidListenersOnDisplay = setParamSetting "forbidListenersOnDisplay"


unsetForbidListenersOnDisplay = unsetSettingByValue "forbidListenersOnDisplay"
         

setParamSetting setting = do 
   e<-createElement' "param"
   setAttribute e "value" setting
   flip appendChild e =<< getElementById' "cache"


unsetSettingByValue setting = do
   e<- getAllWithValue_from =<< getElementById' "cache"
   v<- mapM (flip getAttribute "value") e
   mapM_ (removeElement . snd)
         $ filter ((setting ==) . fst)
         $ zip v e
         
         
peaceTogetherOptions :: [Branch] -> String
peaceTogetherOptions bs = unlines $ map (^.boption) bs


getBranchByName :: [Branch] -> String -> Maybe Branch
getBranchByName bs v = find (\b-> b^.bname==v) bs

getBranchByNameDOM :: String -> Fay (Maybe (String,Element))
getBranchByNameDOM name = return . listToMaybe 
                           =<< flip getAllWithGivenValueFrom name
                           =<< getElementById' "data"

hasValueSet e v = return . any (==v)
                  =<< mapM (flip getAttribute "value") 
                  =<< getAllWithValue_from e 

hasValueSet_l e v = return . any (==v)
                  =<< mapM (flip getAttribute "value") 
                  =<< getAllWithValue_from_l e 



buildFromTemplate c t = do 
   let cDRO = c {cDontRemoveOptions = True}
   let cRO  = c {cDontRemoveOptions = False}
     
   (\pp-> mapM_ (\(l,r)-> l r) 
      $ zip ((displayBranch cRO):dbl cDRO)
      $ catMaybes pp)
      =<< mapM perPair t
   
   
   
   where
   
   dbl c = repeat (displayBranch c)
   
   perPair ("data", byValue) = return $ getBranchByName (c^.cbranches) byValue
   
   perPair (containerID, byValue) = do 
         
      return 
         . Just 
         . (\c-> emptyBranch {bcontent = c})
         . unlines
         =<< mapM (getInnerHtml . snd)
         =<< flip getAllWithGivenValueFrom byValue 
         =<< getElementById' containerID

      
overrideNext :: Context -> Branch -> Fay Branch
overrideNext c b = do 
  b' <- (return
          .fromMaybe b
          . joinMaybe)
          =<< (
                whenJust' (b^.boverrideNext) 
                      $ \on-> flip whenJust' 
                                  (\(_,el)-> return . (\n-> b {bnext = n})
                                     =<< mapM (\e-> return . unpack =<< getValue e) 
                                     =<< children_l el
                                  )
                                  . listToMaybe
                                  =<< flip getAllWithGivenValueFrom on 
                                  =<< getElementById' "cache"
              )
  case (filter (\s-> (filter (\x-> x/=' ') s)/="") $ b'^.bnext) of
    [] -> return b
    _  -> do
            let b = b'
            
            return 
              $ (\n-> b {bnext = n})
              $ map (\(next,skips)-> perBranchSkip skips next)
              $ zip (b^.bnext)
                    $ ((b^.bskipPastNext) ++ (repeat []))
            
            --return b
  
  where
    
    perBranchSkip :: [Int] -> String -> String
    perBranchSkip [] next = next
    perBranchSkip (option:rest) next = maybe next 
      (perBranchSkipContinue rest next . (\b-> takeNth option $ b^.bnext)) 
        $ getBranchByName (c^.cbranches) next
  
    takeNth n l = safeHead $ drop n l
    
    safeHead [] = Nothing
    safeHead l = Just $ head l
    
    perBranchSkipContinue rest next Nothing = next
    perBranchSkipContinue rest next (Just newnext) = perBranchSkip rest newnext


parseBranch :: Element -> Fay Branch
parseBranch e = do 

   nn<- return . unpack =<< getValue e 
   o <- getInnerHtml                   =<< head'          =<< children_l e
   c <- getInnerHtml                   =<< head' . drop 1 =<< children_l e
   n <- mapM parseNext =<< children_l  =<< head' . drop 2 =<< children_l e
   d <- flip hasValueSet_l "displayOnNewPage"    . drop 3 =<< children_l e
   
   ac <- getStringSetting "altContent"           . drop 3 =<< children_l e
   
   act<- getIntSetting "altContentTimeout"       . drop 3 =<< children_l e
   s  <- getIntSetting "autoContinue"            . drop 3 =<< children_l e
   r  <- flip hasValueSet_l "randomNext"         . drop 3 =<< children_l e
   b  <- getMapSetting "buildFromTemplate"       . drop 3 =<< children_l e
   on <- getStringSetting "overrideNext"         . drop 3 =<< children_l e
   spn<- toSkipPastNextSetting 
            =<< getStringSetting "skipPastNext" . drop 3 
            =<< children_l e
   rnc <- getStringSetting "runCode"             . drop 3 =<< children_l e
   
   oc <- getStringSetting "optionChosenState" . drop 3 =<< children_l e
   
   return $ 
      B  {
            bname = nn
           ,boption = o
           ,boptionChosenState = oc
           ,bcontent = c
           ,bnext = n
           ,bdisplayOnNewPage = d
           ,bautoContinue = s
           ,baltContent = ac
           ,baltContentTimeout = act
           ,brandomNext = r
           ,bbuildFromTemplate = b
           ,boverrideNext = on
           ,bskipPastNext = spn
           ,brunCode = rnc
         }
   
   where
   parseNext :: Element -> Fay String
   parseNext e = return . unpack =<< getValue e --getAttr e "value"
   head' = fromMaybe (createTextNode "") . listToMaybe . map return 


getStringSetting setting list = flip whenJust' (getInnerHtml . snd) . listToMaybe 
                     =<< flip getAllWithGivenValueFrom_l setting list
getIntSetting setting list = flip whenJust' (\(_,e)-> do 
                        parseInt . pack =<< getInnerHtml e
                     ) . listToMaybe 
                     =<< flip getAllWithGivenValueFrom_l setting list
getMapSetting setting list = 
   flip whenJust' (\e-> mapM (perPair) =<< (children_l $ snd e)) 
      . listToMaybe 
      =<< flip getAllWithGivenValueFrom_l setting list
   
   where
   perPair e = do 
      v'<- getValue e
      let v = unpack v'
      i'<- getInnerHtml e
      let i = i'
      return (v,i)

toSkipPastNextSetting :: Maybe String -> Fay [[Int]]
toSkipPastNextSetting Nothing   = return []
toSkipPastNextSetting (Just "") = return []
toSkipPastNextSetting (Just s) = mapM (mapM (parseInt.pack) 
                                          . words 
                                          . map commasToSpaces
                                      ) 
                                     $ words s
  where
  commasToSpaces ',' = ' '
  commasToSpaces x = x
  




parseAllBranches :: Fay [Branch]
parseAllBranches = do 
   mapM parseBranch =<< return . nodeListToArray =<< children =<< jsAllBranches
   where
   jsAllBranches :: Fay Element
   jsAllBranches = ffi "document.getElementById(\"data\")"



main :: Fay ()
main = do 
   w<-getWindow 
   addEventListener w "load" setDisplay True






